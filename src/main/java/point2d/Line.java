package point2d;

public class Line {
    protected Point2D start,end;
    public Line (Point2D start, Point2D end) {
        this.start = start;
        this.end = end;
    }
    public Line (float x, float y, float z, float s) {
        this.start = new Point2D(x,y);
        this.end = new Point2D(z,s);
    }

    public Point2D getStart() {
        return start;
    }

    public Point2D getEnd() {
        return end;
    }

    public void setStart(Point2D start) {
        this.start = start;
    }

    public void setEnd(Point2D end) {
        this.end = end;
    }
    public double getLength() {
        return (Math.sqrt (Math.pow (this.end.x - this.start.x, 2) + Math.pow (this.end.y - this.start.y, 2)));
    }
    public Point2D getMiddle() {
        Point2D middle = new Point2D( (this.start.x + this.end.x) /2 , (this.start.y + this.end.y) /2);
        return middle;
    }
}
