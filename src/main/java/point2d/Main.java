package point2d;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Point3D p1 = new Point3D(4,8, 12);
        System.out.println(Arrays.toString(p1.getXYZ()));
        System.out.println(p1);
        Line line = new Line(2F, 3F, 4F, 6F);
        System.out.println(line.getLength());
        System.out.println(line.getMiddle());
    }

}
