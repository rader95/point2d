package task2;

public class Student extends Person {
    private String type;
    private int year;
    private int cost;

    public String getType() {
        return type;
    }

    public int getYear() {
        return year;
    }

    public int getCost() {
        return cost;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Student(String type, int year, int cost) {
        this.type = type;
        this.year = year;
        this.cost = cost;
    }

    public Student(String name, String address, String type, int year, int cost) {
        super(name, address);
        this.type = type;
        this.year = year;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", type='" + type + '\'' +
                ", year=" + year +
                ", cost=" + cost +
                '}';
    }
}
