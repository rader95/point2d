package task2;

public class Staff extends Person{
    private String spec;
    private int salary;
    public Staff(String spec, int salary) {
        this.spec = spec;
        this.salary = salary;
    }

    public String getSpec() {
        return spec;
    }

    public int getSalary() {
        return salary;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Staff{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", spec='" + spec + '\'' +
                ", salary=" + salary +
                '}';
    }
}
