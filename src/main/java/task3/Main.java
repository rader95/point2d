package task3;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle();
        System.out.println(circle);
        Rectangle rectangle = new Rectangle();
        System.out.println(rectangle);
        Square square = new Square("yellow", true, 4);
        System.out.println(square);
    }
}
